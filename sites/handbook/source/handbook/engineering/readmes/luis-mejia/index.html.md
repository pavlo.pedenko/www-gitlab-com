---
layout: markdown_page
title: "Luis Mejia's README"
description: "Personal readme page for Luis Mejia, Backend Engineer, GitLab"
job: "Backend Engineer"
---

## About me

Hi, my name is Luis Mejia and I'm a Backend Engineer based on Nicaragua.
 
I'm a passionate advocate for remote teams, debugging and helping others.

- 🔭 I’m love working with *ruby and python*
- 🌱 I’m currently learning *Go*
- 🤔 I’m looking for: learning and sharing the knowledge
- 💬 What could I talk for 30 minutes about with absolutely no preparation?: parenting, professional/personal growth, personal finances, python
- ⚡ Fun fact 1: I'm a terrible singer but love singing. 
- ⚡ Fun fact 2: I took Boxing classes at high school
- ⚡ Fun fact 3: Planning to retire young
- ⚡ Fun fact 4: PythonNicaragua meetup co-organizer
- ⚡ Fun fact 5: I have 9 dogs 🐕 (6 of which were previously homeless) and plans to add even more after retired

## How you can help me

- I love positive feedback. If I make a mistake please let me know as soon as possible in a direct way. I will really appreciate it
- I'm open minded, let's share ideas

## My working style

- I like helping others, please let me know if you need some help, you won't interrupt me, I'll be happy to help
- If you help me, please expect me to be thankful and let others know, as kindness deserves recognition
- I really appreciate honesty and integrity and I like working with people with similar values.
- I'm a night owl, you may see me online late nights.

## What I assume about others

- I assume honesty and best intentions
- I trust and verify.
- Everybody is in their own pursuit of happiness
