# Data schemas

## Team Members

Please see [`data/team_members/person`](./data/team_members/person#team-member-data-schema) for a detailed account of the person data schema.
